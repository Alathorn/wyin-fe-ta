package com.wyin.smoke;

import com.wyin.base.TestBase;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MyFirstTest extends TestBase {
    @Test
    public void smokeTest() {
        // open WYIN
        driver.get("https://app-ci.spio-wyin.workers.dev/");

        // get text from the top panel
        String header = driver.findElement(By.cssSelector("#root > div > nav > div.nav-central > h1")).getText();

        Assert.assertEquals(header, "WHAT YEAR IS NOW?", "App name found in top of landing page");


        // click the "Sprawdzam" button

        WebElement sprawdzam_btn = driver.findElement(By.cssSelector("div.synchronizer-container:nth-child(2) > button:nth-child(1)"));

        sprawdzam_btn.click();

        String top_number = driver.findElement(By.id("clock")).getText();

        boolean condition = StringUtils.isNumeric(top_number);

        Assert.assertFalse(condition,"The converted clock was identified in modal");



    }

}
